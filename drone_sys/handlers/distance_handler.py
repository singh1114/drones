from math import radians, sin, cos, acos

class DistanceHandler:
    def preprocess_data(self, list):
        """Do some preprocessing with the data."""
        return [radians(float(data)) for data in list]

    def calculate_distance(self, lat1, lon1, lat2, lon2):
        """Calculate the distance between two geo points."""
        data_list = self.preprocess_data([lat1, lon1, lat2, lon2])
        return (
            6371.01 * acos((
                sin(data_list[0]) * sin(
                    data_list[2])) + cos(
                        data_list[0]) * cos(
                            data_list[2]) * cos(
                                data_list[1] - data_list[3])))
