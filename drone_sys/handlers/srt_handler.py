import pysrt

from handlers.constants import VIDEO_FILE


class HandleSRT:
    def open_srt_file(self):
        """Open srt file using pysrt."""
        return pysrt.open(VIDEO_FILE)

    def get_srt_lat_long(self, subs):
        """Get lat long using srt file."""
        lat_lon_list = []
        for sub in subs:
            info = sub.text.split(',')
            lat_lon_list.append({
                'lat': info[1],
                'lon': info[0]
            })
        print(lat_lon_list)
        return lat_lon_list
