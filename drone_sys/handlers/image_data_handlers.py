import os

import sys

from PIL import Image
from PIL.ExifTags import TAGS, GPSTAGS

from handlers.constants import IMAGE_DIR


class HandleImageData:
    def get_exif_data(self, image):
        exif_data = {}
        info = image._getexif()
        if info:
            for tag, value in info.items():
                decoded = TAGS.get(tag, tag)
                if decoded == "GPSInfo":
                    gps_data = {}
                    for gps_tag in value:
                        sub_decoded = GPSTAGS.get(gps_tag, gps_tag)
                        gps_data[sub_decoded] = value[gps_tag]

                    exif_data[decoded] = gps_data
                else:
                    exif_data[decoded] = value

        return exif_data

    def _convert_to_degress(self, value):
        deg_num, deg_denom = value[0]
        d = float(deg_num) / float(deg_denom)

        min_num, min_denom = value[1]
        m = float(min_num) / float(min_denom)

        sec_num, sec_denom = value[2]
        s = float(sec_num) / float(sec_denom)

        return d + (m / 60.0) + (s / 3600.0)

    def get_lat_lon(self, exif_data):
        lat = None
        lon = None

        if "GPSInfo" in exif_data:
            gps_info = exif_data["GPSInfo"]

            gps_latitude = gps_info.get("GPSLatitude")
            gps_latitude_ref = gps_info.get('GPSLatitudeRef')
            gps_longitude = gps_info.get('GPSLongitude')
            gps_longitude_ref = gps_info.get('GPSLongitudeRef')

            if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
                lat = self._convert_to_degress(gps_latitude)
                if gps_latitude_ref != "N":
                    lat *= -1

                lon = self._convert_to_degress(gps_longitude)
                if gps_longitude_ref != "E":
                    lon *= -1

        return lat, lon

    def get_all_lat_lons(self):
        """Get all the lat lons for the image dir."""
        images_lat_long = []
        for file in os.listdir(IMAGE_DIR):
            if file.endswith('.JPG'):
                image = Image.open(IMAGE_DIR + '/' + file)
                exif_data = self.get_exif_data(image)
                lat, lon = self.get_lat_lon(exif_data)
                images_lat_long.append({
                    'lat': lat,
                    'lon': lon,
                    'filename': file
                })
        print(images_lat_long)
        return images_lat_long
