import csv

from handlers.constants import (
    IMAGE_DISTANCE_CSV,
    SRT_MIN_DIS
)
from handlers.distance_handler import DistanceHandler
from handlers.image_data_handlers import HandleImageData
from handlers.srt_handler import HandleSRT


class HandleCSV:
    def create_csv_data(self, srt_data_list, image_data_list):
        """Create CSV file using the data."""
        second = 1
        csv_list = []
        for srt_value in srt_data_list:
            lat1 = srt_value['lat']
            lon1 = srt_value['lon']
            filenames = []
            for image_data in image_data_list:
                lat2 = image_data['lat']
                lon2 = image_data['lon']
                if self.should_calculate_distance((lat1, lat2, lon1, lon2)):
                    dist = DistanceHandler().calculate_distance(
                        lat1, lon1, lat2, lon2)
                else:
                    continue

                if dist * 1000 < SRT_MIN_DIS:
                    filenames.append(image_data['filename'])
            csv_list.append({
                'Time': second,
                'Filenames': filenames
            })
            second = second + 1
        return csv_list

    def should_calculate_distance(self, data_list):
        for data in data_list:
            if not data:
                return False
        return True

    def write_csv_file(self, data_list):
        """Write CSV file with the headers and data."""
        header = ('Time', 'Filenames')
        with open(IMAGE_DISTANCE_CSV, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=header)
            writer.writeheader()

            for data in data_list:
                writer.writerow(data)

    def execute_all_operations(self):
        subs = HandleSRT().open_srt_file()
        srt_data_list = HandleSRT().get_srt_lat_long(subs)

        image_data_list = HandleImageData().get_all_lat_lons()
        data_list = self.create_csv_data(srt_data_list, image_data_list)
        self.write_csv_file(data_list)
        print('\n\nFile written, Please open and have a look.\n\n')
