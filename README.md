# drones

DRONES ARE HERE!

## How to use this codebase.

- Add the extracted zip file in the root directory of the project.
- `$ cd drones_sys`
- Finally run the following code.

```
from handlers.csv_handler import HandleCSV

HandleCSV().execute_all_operations()
```

This will create a simple CSV file with the filenames in distance within 35 m.